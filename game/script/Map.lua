local levity = require "levity"
local LGfx_getHeight = love.graphics.getHeight
local LGfx_setColor = love.graphics.setColor
local LGfx_circle = love.graphics.circle
local LGfx_line = love.graphics.line
local LGfx_translate = love.graphics.translate
local LGfx_origin = love.graphics.origin
local LMath_random = love.math.random
local LMouse_getPosition = love.mouse.getPosition
local M_pi = math.pi
local M_2pi = M_pi*2
local M_abs = math.abs
local M_cos = math.cos
local M_sin = math.sin
local M_max = math.max
local M_min = math.min
local M_sqrt = math.sqrt
local M_fmod = math.fmod
local B_lshift = bit.lshift
local B_and = bit.band
local B_or = bit.bor

local StartArrows = 20
local FireMinDrawStrength = .25
local WindSpeedMin = 16
local WindSpeedMax = 32

local _arrows
local _splits
local _numfired
local _firedarrows = {}
local _nextribboncolor
local _drawstrength
local _windx, _windy
local _winddeltax, _winddeltay
local _windaccelx, _windaccely
local _windchangetimer
local _windchangetime
local _properties
local _windsound
local _drawsound
local _objects
local _touch

local function changeWind()
	local r = LMath_random(WindSpeedMin,WindSpeedMax)
	local a = LMath_random()*M_2pi
	local nextwindx = r*M_cos(a)
	local nextwindy = r*M_sin(a)
	-- nextwind = wind + winddelta0*t + .5*windaccel*t^2
	-- nextwind - wind - winddelta0*t = .5*windaccel*t^2
	-- 2*(nextwind - wind - winddelta0*t) = windaccel*t^2
	-- 2*(nextwind - wind - winddelta0*t)/t^2 = windaccel
	local windchangex = (nextwindx - _windx)
	local windchangey = (nextwindy - _windy)
	local tsq = (_windchangetime*_windchangetime)
	_windaccelx = 2*(windchangex - _winddeltax*_windchangetime)/tsq
	_windaccely = 2*(windchangey - _winddeltay*_windchangetime)/tsq
end

local Map = class()
function Map:_init(map)
	_properties = map.properties

	_objects = map.layers["objects"].objects
	for i = 1, #_objects do
		local object = _objects[i]
		_objects[object.name] = _objects[i]
	end
	_objects.results.visible = false

	love.graphics.setLineWidth(2)
	love.graphics.setLineStyle("rough")
	local r = LMath_random(WindSpeedMin,WindSpeedMax)
	local a = LMath_random()*M_2pi
	_windx = r*M_cos(a)
	_windy = r*M_sin(a)
	r = LMath_random()*WindSpeedMax
	a = LMath_random()*M_2pi
	_winddeltax = r*M_cos(a)
	_winddeltay = r*M_sin(a)
	_windchangetimer = 0
	_windchangetime = 2
	_arrows = StartArrows
	_numfired = 0
	_splits = 0
	changeWind()
	_windsound = levity.bank:play(_properties.windsound)
	_windsound:setLooping(true)
end

function Map:beginMove(dt)
	_windchangetimer = _windchangetimer + dt
	if _windchangetimer >= _windchangetime then
		_windchangetimer = M_fmod(_windchangetimer, _windchangetime)
		changeWind()
	end
	_winddeltax = _winddeltax + _windaccelx*dt
	_winddeltay = _winddeltay + _windaccely*dt
	_windx = _windx + _winddeltax*dt
	_windy = _windy + _winddeltay*dt
	if _drawstrength then
		_drawstrength = M_min(1, _drawstrength + dt)
	end
end

local RibbonColors = {
	{ 0xff, 0x00, 0x00 },
	{ 0xff, 0xff, 0x00 },
	{ 0x00, 0xff, 0x00 },
	{ 0x00, 0xff, 0xff },
	{ 0x80, 0x80, 0xff },
	{ 0xff, 0x00, 0xff },
}

function Map:mousepressed(x, y, button)
	if button ~= 1 then
		return
	end
	if _arrows > 0 then
		_drawstrength = 0
		_nextribboncolor = RibbonColors[LMath_random(#RibbonColors)]
		_drawsound = levity.bank:play(_properties.drawsound)
	end
end

--[[
function Map:mousemoved(x, y, dx, dy)
	if _drawstrength then
		_drawstrength = M_max(0, _drawstrength - M_sqrt(dx*dx + dy*dy)/WindSpeedMax)
	end
end
]]

local function findArrowNear(x, y)
	for i = x-2, x+2 do
		for j = y-2, y+2 do
			local key = B_or(B_lshift(B_and(j, 0xFFFF), 16), B_and(i, 0xFFFF))
			local existingarrow = _firedarrows[key]
			if existingarrow then
				return key, existingarrow
			end
		end
	end
end

local ResultsFormat = [[
Fired %d

Split %d

Press F2 to Restart
]]
function Map:mousereleased(x, y, button)
	if button ~= 1 then
		return
	end
	if _drawstrength and _drawstrength >= FireMinDrawStrength then
		local resistwind = (1-_drawstrength/2)
		x = x + _windx*resistwind
		y = y + _windy*resistwind
		local arrow
		local key, existingarrow = findArrowNear(x, y)
		if existingarrow then
			arrow = existingarrow
			_splits = _splits + 1
			_firedarrows[key] = nil
			levity.bank:play(_properties.splitsound)
		else
			arrow = {}
			_arrows = _arrows - 1
			arrow.x = x
			arrow.y = y
			arrow.color = _nextribboncolor
			key = B_or(B_lshift(B_and(y, 0xFFFF), 16), B_and(x, 0xFFFF))
			_firedarrows[key] = arrow
		end
		_numfired = _numfired + 1
		levity.bank:play(_properties.shotsound)

		if _arrows < 1 then
			_objects.results.text = string.format(ResultsFormat, _numfired, _splits)
			_objects.results.visible = true
		end
	end
	if _drawsound then
		_drawsound:stop()
		_drawsound = nil
	end
	_drawstrength = nil
	_nextribboncolor = nil
end

function Map:touchpressed(touch, x, y, button)
	if _touch then
		return
	end
	_touch = touch
	self:mousepressed(x, y, 1)
end

function Map:touchreleased(touch, x, y)
	if _touch ~= touch then
		return
	end
	_touch = nil
	self:mousereleased(x, y, 1)
end

local function drawFlutteringRibbon(x, y, resistwind)
	resistwind = resistwind or 1
	local windx = _windx/5
	local windy = _windy/5
	local winddeltax = _winddeltax/5
	local winddeltay = _winddeltay/5
	for j = 4, 0, -1 do
		local t = j/5
		local x2 = x + resistwind*(windx + winddeltax*t)
		local y2 = y + resistwind*(windy + winddeltay*t)
		x2 = x2 + LMath_random()*8*resistwind
		y2 = y2 + LMath_random()*8*resistwind
		LGfx_line(x, y, x2, y2)
		x, y = x2, y2
	end
end

local ArrowLine = {
	-- fletching rect 0, 0 to 16, -24
	-- shaft rect 7, -8 to 9, -72
	-- head rect 4, -72 to 12, -96
	0, 0,
	7, -8,
	9, -8,
	16, 0,
	16, -16,
	9, -24,
	9, -72,
	12, -76,
	8, -96,
	4, -76,
	7, -72,
	7, -24,
	0, -16,
	0, 0
}

function Map:endDraw()
	for _, arrow in pairs(_firedarrows) do
		local x = arrow.x
		local y = arrow.y
		LGfx_circle("fill", x, y, 4)
	end
	for _, arrow in pairs(_firedarrows) do
		local x = arrow.x
		local y = arrow.y
		LGfx_setColor(arrow.color)
		drawFlutteringRibbon(x, y)
	end

	if _drawstrength and _drawstrength >= FireMinDrawStrength then
		local x, y = LMouse_getPosition()
		local resistwind = (1-_drawstrength/2)
		LGfx_setColor(_nextribboncolor)
		drawFlutteringRibbon(x, y, resistwind)
	end

	LGfx_setColor(0xff, 0xff, 0xff)

	LGfx_translate(0, LGfx_getHeight())
	for i = 1, _arrows do
		LGfx_line(ArrowLine)
		LGfx_translate(16, 0)
	end

	LGfx_origin()
end

function Map:keypressed(key)
	if key == "f2" then
		levity:setNextMap(levity.mapfile)
	elseif key == "escape" then
		love.event.quit()
	end
end

return Map
