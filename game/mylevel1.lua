return {
  version = "1.2",
  luaversion = "5.1",
  tiledversion = "1.2.2",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 25,
  height = 20,
  tilewidth = 32,
  tileheight = 32,
  nextlayerid = 8,
  nextobjectid = 41,
  properties = {
    ["drawsound"] = "draw_bow_sound-mike-koenig-1234.ogg",
    ["script"] = "Map",
    ["shotsound"] = "Throw_Knife-Anonymous-1894795848.ogg",
    ["splitsound"] = "31813__malexmedia__malexmedia-twigsnapb.ogg",
    ["windsound"] = "Wind-Mark_DiAngelo-1940285615.ogg"
  },
  tilesets = {},
  layers = {
    {
      type = "objectgroup",
      id = 7,
      name = "objects",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      draworder = "topdown",
      properties = {},
      objects = {
        {
          id = 40,
          name = "results",
          type = "",
          shape = "text",
          x = 0,
          y = 352,
          width = 800,
          height = 192,
          rotation = 0,
          visible = true,
          text = "Fired XX\n\nSplits XX\n\nPress F2 to Restart",
          fontfamily = "Sans Serif",
          pixelsize = 32,
          wrap = true,
          color = { 255, 255, 255 },
          halign = "center",
          properties = {}
        }
      }
    }
  }
}
